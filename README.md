# SAS Markdown

This repo houses examples of running SAS within an R Markdown document, potentially useful for implementing Git tracking and more reproducible output for analyses that use SAS. 

The file SAP_SAS.Rmd contains a combined SAP and analysis based on Tina Davenport's Rmd SAP template. 
